package net.toften.mavencontainer.discovery;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.prop4j.BadPropertyInfoException;
import net.toften.prop4j.Prop;
import net.toften.prop4j.config.ManualConfig;
import net.toften.prop4j.config.Prop4jConfig;
import net.toften.prop4j.loaders.PropertyLoader;
import net.toften.prop4j.loaders.PropertyLoaderInitException;

import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectUtils;
import org.junit.Before;
import org.junit.Test;


public class ApplicationTest {
    private Map<String, String> props = new HashMap<String, String>();
    
    {
        props.put("net.toften.mavencontainer.maven.repopath", "/Users/thomaslarsen/.m2/repository");
        props.put("net.toften.mavencontainer.maven.repopath.start", "/com/ubsw");
    }
    
    @Before
    public void setupProperties() {
        Prop4jConfig config = new ManualConfig();
        
        config.addLoader(new PropertyLoader() {
            
            @Override
            public void setProperty(String name, Object value) throws UnsupportedOperationException {
                throw new UnsupportedOperationException("Property " + name + " cannot be set");
            }
            
            @Override
            public boolean isMutable() {
                return false;
            }
            
            @Override
            public void init(Properties loaderProperties) throws BadPropertyInfoException, PropertyLoaderInitException { }
            
            @Override
            public Object getProperty(String name) {
                return props.get(name);
            }
        }, null);
        
        Prop.init(config);
    }
    
    @Test
    public void testLoad() throws Exception {
        Application.findApplications();
    }
}
