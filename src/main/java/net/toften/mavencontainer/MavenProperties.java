package net.toften.mavencontainer;

import net.toften.prop4j.PropertyBase;
import net.toften.prop4j.PropertyGetter;

public interface MavenProperties extends PropertyBase {
    @PropertyGetter (name = "net.toften.mavencontainer.maven.repopath")
    String getMavenRepositoryPath();

    @PropertyGetter (name = "net.toften.mavencontainer.launcherconfig.jaxbcontext", defaultValue = "launcher.config")
    String getLaunchconfigJAXBContext();

    @PropertyGetter (name = "net.toften.mavencontainer.launcherconfig.containerpath", defaultValue = "META-INF/container/")
    String getContainerJarPath();

    @PropertyGetter (name = "net.toften.mavencontainer.launcherconfig.launchfilename", defaultValue = "launch.xml")
    String getContainerLaunchFilename();

    @PropertyGetter (name = "net.toften.mavencontainer.maven.repopath.start")
    String getStartAt();
}
