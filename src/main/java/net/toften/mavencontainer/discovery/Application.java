package net.toften.mavencontainer.discovery;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.factory.DefaultArtifactFactory;
import org.apache.maven.artifact.metadata.ArtifactMetadataRetrievalException;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.DefaultArtifactRepository;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.resolver.DefaultArtifactResolver;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.DependencyManagement;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.artifact.MavenMetadataSource;
import org.apache.maven.settings.DefaultMavenSettingsBuilder;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import launcher.config.App;
import launcher.config.Launcher;
import net.toften.mavencontainer.MavenProperties;
import net.toften.prop4j.Prop;

public class Application {
    private static Application a;

    private static MavenProperties prop;

    public Application(String id, String clazz, String method) {
        System.out.println("***** " + id + "/" + clazz + "/" + method + " ******");
    }

    public static void findApplications() throws Exception {
        Artifact a = new DefaultArtifactFactory().createProjectArtifact("net.toften", "processor-maven-plugin", "1.0-SNAPSHOT");
        resolveDeps(a);
        prop = Prop.getProp(MavenProperties.class);

        File repoDir = new File(prop.getMavenRepositoryPath() + prop.getStartAt());
        if (!repoDir.exists())
            throw new Exception("Repo doesn't exist");
        if (!repoDir.isDirectory())
            throw new Exception("Repo is not a dir");

        JAXBContext jbc = JAXBContext.newInstance(prop.getLaunchconfigJAXBContext());
        Unmarshaller unmarshaller = jbc.createUnmarshaller();

        List<Application> apps = new ArrayList<Application>();
        analyseDir(repoDir, apps, unmarshaller);
    }

    private static void analyseDir(File dir, List<Application> apps, Unmarshaller unmarshaller) throws Exception {
        if (!dir.isDirectory())
            throw new Exception("Repo is not a dir");

        for (File e : dir.listFiles()) {
            if (e.isDirectory())
                analyseDir(e, apps, unmarshaller);

            if (e.getName().endsWith("jar")) {
                List<Application> newApps = analyseJar(e, unmarshaller);
                if (newApps != null)
                    apps.addAll(newApps);
            }
        }
    }

    private static Model analysePom(InputStream pomIs) throws IOException, XmlPullParserException {
        MavenXpp3Reader pomReader = new MavenXpp3Reader();

        return pomReader.read(pomIs);
    }

    private static List<Application> analyseJar(File e, Unmarshaller unmarshaller) throws Exception {
        List<Application> newApps = null;

        JarFile jf = new JarFile(e);

        ZipEntry c = jf.getEntry(prop.getContainerJarPath());
        if (c != null) {
            if (!c.isDirectory())
                throw new Exception(prop.getContainerJarPath() + " is not a dir");

            // Get launch config
            ZipEntry lnch = jf.getEntry(prop.getContainerJarPath() + prop.getContainerLaunchFilename());
            if (lnch != null && lnch.getSize() > 0) {
                InputStream is = jf.getInputStream(lnch);
                App foundApp = (App)unmarshaller.unmarshal(is);
                newApps = analyseApp(foundApp);
                // Get POM config
                String mavenInfo = e.getAbsolutePath().substring(prop.getMavenRepositoryPath().length() + 1);
                //            System.out.println(mavenInfo);
                String[] parts = mavenInfo.split("/");
                String version = parts[parts.length - 2];
                String artefactId = parts[parts.length - 3];
                String groupId = "";
                for (int i = 0; i < parts.length - 3; i++) {
                    groupId = groupId + (groupId.length() > 0 ? "." : "") + parts[i];
                }
                //            System.out.println("G: " + groupId + "/A: " + artefactId + "/V: " + version);

                String pomPath = "META-INF/maven/" + groupId + "/" + artefactId + "/pom.xml";
                ZipEntry pom = jf.getEntry(pomPath);
                if (pom != null && pom.getSize() > 0) {
                    InputStream pomIs = jf.getInputStream(pom);
                    Model m = analysePom(pomIs);

                    for (Dependency d : m.getDependencies()) {
                        System.out.println(d.getGroupId() + "/" + d.getArtifactId() + "/" + d.getVersion());
                    }

                    MavenProject project = new MavenProject(m);
                    List<Dependency> pDeps = project.getDependencies();
                    System.out.println("Deps size: " + pDeps.size());
                    for (Dependency dep : pDeps) {
                        System.out.println(dep.getGroupId() + "/" + dep.getArtifactId() + "/" + dep.getVersion());
                    }
                    
                    project.resolveActiveArtifacts();
                    Set<Artifact> arts = project.getArtifacts();
                    if (arts != null) {
                        System.out.println("Arts size: " + arts.size());
                        for (Artifact artifact : arts) {
//                            resolveDeps(artifact);
                            System.out.println("Dependency " + artifact.getFile().getAbsolutePath() + " added. File does " + (artifact.getFile().exists() ? "" : "NOT ") + "exist.");
                        }
                    }
                    else System.out.println("DEPS null");
                }
            }
        }

        return newApps;
    }
    
    private static void resolveDeps(Artifact a) throws IOException, XmlPullParserException, ArtifactMetadataRetrievalException {
        ArtifactResolver resolver = new DefaultArtifactResolver();
        Settings s = new DefaultMavenSettingsBuilder().buildSettings();
        ArtifactRepository rep = new DefaultArtifactRepository("local", s.getLocalRepository(), new DefaultRepositoryLayout());
        
        ArtifactMetadataSource source = new MavenMetadataSource(); 
        
        System.out.println(source.retrieveAvailableVersions(a, rep, null));
    }

    private static List<Application> analyseApp(App app) {
        List<Application> apps = new ArrayList<Application>();

        for (Launcher l : app.getLaunchers().getLauncher()) {
            a = new Application(l.getId(), l.getClazz(), l.getMethod());
            apps.add(a);
        }

        return apps;
    }
}